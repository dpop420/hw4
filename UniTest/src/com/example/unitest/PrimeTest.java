package com.example.unitest;

import junit.framework.TestCase;

public class PrimeTest extends TestCase {
	public void test(){
		assertTrue(Prime.isPrime(4));
		assertFalse(Prime.isPrime(2));
	}
}
