package rotate;

import static org.junit.Assert.assertEquals;
import rotate.STEP3;

import org.junit.Test;


public class STEP3Test {
	
	@Test
	public void test() {
		assertEquals("eabcd", STEP3.rotate("abcde", 1));
		assertEquals("deabc", STEP3.rotate("abcde", 2));
		assertEquals("abcde", STEP3.rotate("abcde", 5));
		assertEquals("cdeab", STEP3.rotate("abcde", -2));
		assertEquals("eabcd", STEP3.rotate("abcde", -4));
		assertEquals("abcde", STEP3.rotate("abcde", -5));
	}
}
