package rotate;

public class STEP3 {
	public static void main(String args[]){
		System.out.println("入力前：" + args[0]);
		System.out.println("入力後：" + rotate(args[0], Integer.parseInt(args[1])));
	}
	
	public static String rotate(String input, int n){
		String output = "";
		if(n > input.length()){
			return("文字列の長さ以上の指定はできません");
		}
		if (n > 0){
			String str1 = input.substring(0, input.length() - n);
			String str2 = input.substring(input.length() - n, input.length());
			output = str2 + str1;
		}else{
			String str1 = input.substring(0, -n);
			String str2 = input.substring(-n, input.length());
			output = str2 + str1;
		}
		
		return output;
	}
}
